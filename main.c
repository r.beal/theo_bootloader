/*******************************************************************************
Copyright 2016 Microchip Technology Inc. (www.microchip.com)

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

To request to license the code under the MLA license (www.microchip.com/mla_license), 
please contact mla_licensing@microchip.com
*******************************************************************************/

/** INCLUDES *******************************************************/
#include "system.h"
#include "system_config.h"

#include <usb.h>
#include <usb_device_hid.h>


#include <app_hid_boot_loader.h>
#include <app_led_usb_status.h>

void init(void);
void ioInit(void);
void timer(void);

char chienne;

MAIN_RETURN main(void)
{
    
    INTCON2bits.ALTIVT = 1;
     
    chienne = 0;
    RCONbits.SWDTEN = 0;
    
    init();
    ioInit();
    timer();
    
    while (chienne == 0)
        ClrWdt();
    
    SYSTEM_Initialize(SYSTEM_STATE_USB_START);

    USBDeviceInit();
    USBDeviceAttach();
        

    while(1)
    {
        
        //In case the application uses config bit settings that enable the WDT, we 
        //need to periodically clear it in order for the bootloader to run normally.
        ClrWdt();
        
        SYSTEM_Tasks();

        #if defined(USB_POLLING)
            // Interrupt or polling method.  If using polling, must call
            // this function periodically.  This function will take care
            // of processing and responding to SETUP transactions
            // (such as during the enumeration process when you first
            // plug in).  USB hosts require that USB devices should accept
            // and process SETUP packets in a timely fashion.  Therefore,
            // when using polling, this function should be called
            // regularly (such as once every 1.8ms or faster** [see
            // inline code comments in usb_device.c for explanation when
            // "or faster" applies])  In most cases, the USBDeviceTasks()
            // function does not take very long to execute (ex: <100
            // instruction cycles) before it returns.
            USBDeviceTasks();
        #endif

        /* If the USB device isn't configured yet, we can't really do anything
         * else since we don't have a host to talk to.  So jump back to the
         * top of the while loop. */
        if( USBGetDeviceState() < CONFIGURED_STATE )
        {
            /* Jump back to the top of the while loop. */
            continue;
        }

        /* If we are currently suspended, then we need to see if we need to
         * issue a remote wakeup.  In either case, we shouldn't process any
         * keyboard commands since we aren't currently communicating to the host
         * thus just continue back to the start of the while loop. */
        if( USBIsDeviceSuspended() == true )
        {
            /* Jump back to the top of the while loop. */
            continue;
        }

        //Application specific tasks
        APP_HIDBootLoaderTasks();

    }//end while
}//end main


bool USER_USB_CALLBACK_EVENT_HANDLER(int event, void *pdata, uint16_t size)
{
    switch( event )
    {
        case EVENT_TRANSFER:
            break;

        case EVENT_SOF:
            /* We are using the SOF as a timer to time the LED indicator.  Call
             * the LED update function here. */
            APP_LEDUpdateUSBStatus();
            break;

        case EVENT_SUSPEND:
            /* Update the LED status for the suspend event. */
            APP_LEDUpdateUSBStatus();
            break;

        case EVENT_RESUME:
            /* Update the LED status for the resume event. */
            APP_LEDUpdateUSBStatus();
            break;

        case EVENT_CONFIGURED:
            /* When the device is configured, we can (re)initialize the demo
             * code. */
            APP_HIDBootLoaderInitialize();
            break;

        case EVENT_SET_DESCRIPTOR:
            break;

        case EVENT_EP0_REQUEST:
            /* We have received a non-standard USB request.  The HID driver
             * needs to check to see if the request was for it. */
            USBCheckHIDRequest();
            break;

        case EVENT_BUS_ERROR:
            break;

        case EVENT_TRANSFER_TERMINATED:
            break;

        default:
            break;
    }
    return true;
}

void init(void) {
    
    OSCCONbits.NOSC = 0b001;
    OSCCONbits.OSWEN = 1;
    
    
    CLKDIVbits.RCDIV = 0;
    CLKDIVbits.CPDIV = 0;
    CLKDIVbits.PLLEN = 1;
    
    OSCTUNbits.STEN = 1;
    OSCTUNbits.STSRC = 1;
    
    while ( !OSCCONbits.LOCK && OSCCONbits.COSC != 0b001);
    
}

void ioInit(void) {
    ANSA = 0;
    ANSB = 0;
    ANSC = 0;
    
    TRISBbits.TRISB2 = 1;
}

void timer(void) {
    
    TMR1 = 0;
    PR1 = 0xFFFF;
    IPC0bits.T1IP = 1;
    T1CONbits.TCS = 0;
    T1CONbits.TCKPS = 0b00;
    T1CONbits.TON = 1;
    
    IEC0bits.T1IE = 1;
}

void __attribute__((interrupt,no_auto_psv)) _AltT1Interrupt( void )
{
    IFS0bits.T1IF = 0;
    chienne = 1;
    
    T1CONbits.TON = 0;
}

/*******************************************************************************
 End of File
*/

